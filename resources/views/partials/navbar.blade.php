<header>
    <nav x-data="{ open: false}" class="{{ session('theme') === 'dark' ? 'dark:bg-stone-950' : 'bg-primary-950' }}">
        <div class="mx-auto max-w-5xl">
            <div class="relative flex h-28 items-center justify-between">
                <div class="absolute top-20 flex items-center sm:hidden">
                    <!-- Mobile menu button-->
                    <button @click="open = !open" type="button"
                            class="button-menu-movil"
                            aria-controls="mobile-menu" aria-expanded="false">
                        <span class="absolute -inset-0.5"></span>
                        <span class="sr-only"></span>
                        <svg x-show="!open" class="size-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5" />
                        </svg>
                        <svg x-show="open" class="size-6" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M6 18L18 6M6 6l12 12" />
                        </svg>
                    </button>
                </div>
                <div class="flex flex-1 items-center justify-center">
                    <div class="flex {{ (Route::is('home')) ? 'logo-active' : 'logo' }}">
                        <a href="{{ route('home') }}">
                            <img class="h-24 w-24" src="{{ asset('storage/logo.png') }}" alt="Logo">
                        </a>
                    </div>
                    <div class="text-lg text-center hidden sm:ml-6 sm:block mt-3">
                        <div class="flex space-x-4">
                            <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                            <a href="{{ route('services') }}"
                               class="px-3 py-2 {{ (Route::is('services')) ? 'active' : 'link' }}">
                                {{ __('Services') }}
                            </a>
                            <a href="{{ route('portfolio') }}"
                               class="px-3 py-2 {{ (Route::is('portfolio')) ? 'active' : 'link' }}">
                                {{ __('Portfolio') }}
                            </a>
                            <a href="{{ route('skills') }}"
                               class="px-3 py-2 {{ (Route::is('skills')) ? 'active' : 'link' }}">
                                {{ __('Skills') }}
                            </a>
                            <div class="inline-flex">
                                <a href="{{route('set_language', ['es'])}}">
                                    <img class="h-6 w-6 mr-2 mt-2.5" src="{{ asset('storage/Spain_flag.png') }}" alt="Spain flag">
                                </a>
                                <a href="{{route('set_language', ['en'])}}">
                                    <img class="h-6 w-6 mt-2.5" src="{{ asset('storage/Unitedstates_flag.png') }}" alt="United states flag">
                                </a>
                            </div>
                            <form action="{{route('theme')}}" method="POST">
                                @csrf
                                <label class="{{ session('theme') === 'dark' ? 'text-sun hover:text-stone-100' : 'text-stone-400 hover:text-stone-100' }} mb-0.5 cursor-pointer">
                                    <input class="appearance-none w-1.5 h-1.5" type="checkbox" name="theme" value="{{ session('theme') === 'dark' ? 'light' : 'dark' }}" onchange="this.form.submit()">
                                    <i class="{{ session('theme') === 'light' ? 'fa-regular fa-moon' : 'fa-regular fa-sun' }} mt-3.5"></i>
                                </label>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Mobile menu, show/hide based on menu state. -->
        <div class="sm:hidden block" x-show="open" x-on:click.away="open = false">
            <div class="text-lg space-y-1 px-2">
                <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                <a href="{{ route('services') }}"
                   class="px-3 py-1 block {{ (Route::is('services')) ? 'active' : 'link' }}">
                    {{ __('Services') }}
                </a>
                <a href="{{ route('portfolio') }}"
                   class="px-3 py-1 block {{ (Route::is('portfolio')) ? 'active' : 'link' }}">
                    {{ __('Portfolio') }}
                </a>
                <a href="{{ route('skills') }}"
                   class="px-3 py-1 block {{ (Route::is('skills')) ? 'active' : 'link' }}">
                    {{ __('Skills') }}
                </a>
            </div>
            <div>
                <a href="{{route('set_language', ['es'])}}">
                    <img class="h-8 w-8 ml-5" src="{{ asset('storage/Spain_flag.png') }}" alt="Spain flag">
                </a>
                <a href="{{route('set_language', ['en'])}}">
                    <img class="h-8 w-8 ml-5" src="{{ asset('storage/Unitedstates_flag.png') }}" alt="United states flag">
                </a>
            </div>
            <form action="{{route('theme')}}" method="POST" class="ml-6">
                @csrf
                <label class="{{ session('theme') === 'dark' ? 'text-sun hover:text-stone-100' : 'text-stone-400 hover:text-stone-100' }} mb-0.5">
                    <input class="appearance-none" type="checkbox" name="theme" value="{{ session('theme') === 'dark' ? 'light' : 'dark' }}" onchange="this.form.submit()">
                    <i class="{{ session('theme') === 'light' ? 'fa-regular fa-moon' : 'fa-regular fa-sun' }} mt-2.5 mb-4"></i>
                </label>
            </form>
        </div>
    </nav>
</header>
