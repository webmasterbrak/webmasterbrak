<?php

use App\Http\Controllers\OptionController;
use App\Http\Controllers\LegalController;
use App\Http\Controllers\MenuController;
use Illuminate\Support\Facades\Route;

Route::controller(MenuController::class)->group(function () {
    Route::get('/', 'index')->name('home');
    Route::get('/services', 'services')->name('services');
    Route::get('/portfolio', 'portfolio')->name('portfolio');
    Route::get('/skills', 'skills')->name('skills');
    Route::get('/studies', 'studies')->name('studies');
});

Route::controller(LegalController::class)->group(function () {
    Route::get('/legal-disclaimer', 'disclaimer')->name('legal');
    Route::get('/cookies', 'cookies')->name('cookies');
});

Route::controller(OptionController::class)->group(function () {
    Route::get('/set_language/{language}', 'setLanguage')->name( 'set_language');
    Route::post('/theme', 'updateTheme')->name('theme');
    Route::get('/consent', 'setConsent')->name('consent');
    Route::get('/refuse', 'refuse')->name('refuse');
    Route::get('/refuse-banner', 'refuseBanner')->name('refuse-banner');
    Route::get('/curriculum', 'curriculum')->name('curriculum');
});
