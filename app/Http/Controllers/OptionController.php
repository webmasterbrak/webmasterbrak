<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class OptionController extends Controller
{
    public function setLanguage($language)
    {
        SEOMeta::addMeta('robots', 'none');

        if(array_key_exists($language, config('languages')))
        {
            session()->put('applocale', $language);
            if ($language == 'en') {
                return back()->with('success', 'Language changed successfully!!');
            }else{
                return back()->with('success', 'Idioma cambiado correctamente!!');
            }
        }
        return back()->with('error', __('Language not found!!'));
    }

    public function updateTheme(Request $request)
    {
        session()->put('theme', $request->input('theme'));
        return back()->with('success', __('Theme changed successfully!!'));
    }

    public function setConsent()
    {
        session()->put('consent', true);
        return back();
    }

    public function refuse()
    {
        session()->put('consent', false);
        session()->put('theme', 'dark');
        session()->put('applocale', 'es');
        return back();
    }

    public function refuseBanner()
    {
        session()->put('consent', 'refused');
        return back();
    }

    public function curriculum()
    {
        SEOMeta::addMeta('robots', 'none');

        $pdf = Pdf::loadView('curriculum');
        return $pdf->stream('cv_webmasterbrak.pdf');
    }
}
