@extends('components.layouts.app')

@section('title', __('Projects'))

@section('content')
    <main class="bg-main">
        <div class="content pb-3">
            <div class="text-primary-200 dark:text-primary-300">
                {{ Breadcrumbs::render('portfolio') }}
            </div>
            <div class="flex justify-center h1">
                <div class="flex-none">
                    <i class="fa-solid fa-folder-tree text-lg"></i>
                </div>
                <h1 class="h1 ml-2 mt-1">
                    {{ __('Portfolio') }}
                </h1>
            </div>
            <section class="mx-6">
                <p class="text my-6">
                    {{ __('Discover how I build innovative digital experiences. My portfolio presents a selection of projects ranging from static websites to interactive web applications.') }}
                </p>
                @foreach($projects as $project)
                <div class="card">
                    <div class="relative rounded-r-none rounded-xl sm:shrink-0">
                        <img class="sm:w-72 sm:h-full object-cover" src="{{asset($project->image)}}" alt="project-image"/>
                    </div>
                    <div class="p-3">
                        <div class="flex">
                            <div class="block mb-4 font-semibold uppercase">
                                {{ __('In') }} {{ __($project->status) }}
                            </div>
                        </div>
                        <h4 class="block mb-2 text-2xl font-semibold">
                            {{ __($project->title) }}
                        </h4>
                        <p class="block mb-8">
                            {{ __($project->description) }}
                        </p>
                        <div class="flex">
                            <div class="block mb-2">
                                {{ __('Technologies') }}:
                            </div>
                            <p class="ml-1 uppercase">
                                {{ __($project->technologies) }}
                            </p>
                        </div>
                        <div class="flex">
                            <div class="block mb-2">
                                Web:
                            </div>
                            @if($project->link)
                                <a href="{{$project->link}}" target="_blank"
                                   class="ml-1 card-link">
                                    {{ __($project->link) }}
                                </a>
                            @else
                                <div class="ml-1">
                                    {{ __('Not available.') }}
                                </div>
                            @endif
                        </div>
                        <div class="flex">
                            <div class="block mb-2">
                                Git:
                            </div>
                            @if($project->git === 'Private')
                                <div class="ml-1">
                                    {{ __('Private.') }}
                                </div>
                            @elseif($project->git === null)
                                <div class="ml-1">
                                    {{ __('Not available.') }}
                                </div>
                            @else
                                <a href="{{$project->git}}" target="_blank"
                                   class="ml-1 card-link">
                                    {{ __($project->git) }}
                                </a>
                            @endif
                        </div>
                        <div class="flex">
                            <div class="block mb-2">
                                {{ __('Type') }}:
                            </div>
                            <p class="ml-1 capitalize">
                                {{ __($project->type) }}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
            </section>
        </div>
    </main>
@endsection
