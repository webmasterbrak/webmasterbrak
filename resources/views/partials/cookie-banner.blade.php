<section>
    @if(session('consent') === false)
        <div x-data="{ showBanner: true }" x-show="showBanner" class="bg-primary-200 dark:bg-primary-800 p-4 fixed bottom-0 w-full">
            <p class="text-center">
                {{ __('We use our own cookies to improve your browsing experience. By continuing browsing, you accept their use. You can get more information about our') }}
                <a href="{{route('cookies')}}" class="link">
                    <span class="font-bold">{{ __('cookies policy') }}</span>
                </a>
            </p>
            <div class="flex flex-col sm:flex-row md:max-w-[48rem] md:mx-auto">
                <a @click="showBanner = false" class="btn text-center rounded mt-2 w-full sm:mx-3"
                   type="button" href="{{route('consent')}}">
                    {{ __('Accept') }}
                </a>
                <div x-cloak x-data="{ 'showModal': false }" @keydown.escape="showModal = false"
                     class="showModal ? 'overflow-hidden' : 'overflow-visible'">
                    <!-- Trigger for Modal -->
                    <div @click="showModal = true" class="btn cursor-pointer text-center rounded mt-2 sm:w-60 w-full sm:mx-3">
                        {{ __('Configure') }}
                    </div>
                    <!-- Modal -->
                    <div x-cloak x-show="showModal" class="modal">
                        <!-- Modal inner -->
                        <div class="modal-size" @click.away="showModal = false"
                             x-transition:enter="motion-safe:ease-out duration-300"
                             x-transition:enter-start="opacity-0 scale-90"
                             x-transition:enter-end="opacity-100 scale-100">
                            <!-- Title / Close-->
                            <div class="flex items-center justify-between">
                                <h5 class="text-stone-950 text-xl mx-auto max-w-none">
                                    {{ __('Cookie settings panel') }}
                                </h5>
                                <button type="button" class="close" @click="showModal = false">
                                    <i class="fa-solid fa-xmark"></i>
                                </button>
                            </div>
                            <!-- content -->
                            <div class="text-stone-800 mt-3 h-auto">
                                <div class="flex text-center text-xs">
                                    {{ __('This is the advanced configurator of own and third party cookies. Here you can modify parameters that will directly affect your browsing experience on this website.') }}
                                </div>
                                <div class="flex flex-col w-full h-auto">
                                    <div class="flex w-full items-center bg-stone-200 border-b border-stone-300 rounded-3xl mt-3 px-5 py-1 text-sm">
                                        <div class="flex-1">
                                            {{ __('Strictly necessary cookies') }}
                                        </div>
                                        <!--Default switch without label-->
                                        <label>
                                            <input class="switch" type="checkbox" role="switch" checked disabled/>
                                        </label>
                                    </div>
                                    <div x-data="{ switchNecesaryCookies: false }" class="mb-3">
                                        <p class="text-sm">
                                            {{ __('They enable basic functions such as page navigation and access to secure areas. The website cannot function properly without these cookies (which is why they cannot be disabled).') }}
                                        </p>
                                        <div @click="switchNecesaryCookies = !switchNecesaryCookies" class='flex items-center text-stone-600 w-full border-b overflow-hidden md:mt-0 mb-2 mx-auto'>
                                            <div class='w-10 border-r px-2 transform transition duration-300 ease-in-out' :class="{'rotate-90': switchNecesaryCookies,' -translate-y-0.0': !switchNecesaryCookies }">
                                                <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                                </svg>
                                            </div>
                                            <div class='flex items-center px-2 pt-1'>
                                                <div class='mx-3'>
                                                    <button class="link">
                                                        {{ __('Show complete list') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex p-5 md:p-0 w-full transform transition duration-300 ease-in-out border-b pb-10"
                                             x-cloak x-show="switchNecesaryCookies" x-collapse x-collapse.duration.500ms >
                                            <table class="table-auto text-sm">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        {{ __('Name') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Provider') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Purpose') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Expiration') }}
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        {{ '_token' }}
                                                    </td>
                                                    <td>
                                                        {{ __('Own') }}
                                                    </td>
                                                    <td>
                                                        {{ __('It facilitates secure visitor navigation by preventing cross-site request forgery (CSRF). This cookie is essential for the security of the website and the visitor.') }}
                                                    </td>
                                                    <td>
                                                        {{ __('Session') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {{ '_previous' }}
                                                    </td>
                                                    <td>
                                                        {{ __('Own') }}
                                                    </td>
                                                    <td>
                                                        {{ __('This cookie is essential for proper navigation of the website.') }}
                                                    </td>
                                                    <td>
                                                        {{ __('Session') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        {{ '_flash' }}
                                                    </td>
                                                    <td>
                                                        {{ __('Own') }}
                                                    </td>
                                                    <td>
                                                        {{ __('This cookie is necessary to display the informative notices of the website.') }}
                                                    </td>
                                                    <td>
                                                        {{ __('Session') }}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="flex w-full items-center bg-stone-200 border-b border-stone-300 rounded-3xl mt-3 px-5 py-1 text-sm">
                                        <div class="flex-1">
                                            {{ __('Cookies of preferences') }}
                                        </div>
                                        <!--Default switch without label-->
                                        <label>
                                            <input class="switch" type="checkbox" role="switch" checked disabled />
                                        </label>
                                    </div>
                                    <div x-data="{ switchPreferencesCookies: false }" class="mb-3">
                                        <p class="text-sm">
                                            {{ __('They store user preferences. For example, if a user has selected a preferred language (they cannot be disabled).') }}
                                        </p>
                                        <div @click="switchPreferencesCookies = !switchPreferencesCookies" class='flex items-center text-stone-600 w-full border-b overflow-hidden md:mt-0 mb-2 mx-auto'>
                                            <div class='w-10 border-r px-2 transform transition duration-300 ease-in-out' :class="{'rotate-90': switchPreferencesCookies,' -translate-y-0.0': !switchPreferencesCookies }">
                                                <svg fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-6 h-6">
                                                    <path stroke-linecap="round" stroke-linejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
                                                </svg>
                                            </div>
                                            <div class='flex items-center px-2 pt-1'>
                                                <div class='mx-3'>
                                                    <button class="link">
                                                        {{ __('Show complete list') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="flex p-5 md:p-0 w-full transform transition duration-300 ease-in-out border-b pb-10"
                                             x-cloak x-show="switchPreferencesCookies" x-collapse x-collapse.duration.500ms >
                                            <table class="table-auto text-sm">
                                                <thead>
                                                <tr>
                                                    <th>
                                                        {{ __('Name') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Provider') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Purpose') }}
                                                    </th>
                                                    <th>
                                                        {{ __('Expiration') }}
                                                    </th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td class="w[2rem]">
                                                        {{ 'applocale' }}
                                                    </td>
                                                    <td>
                                                        {{ __('Own') }}
                                                    </td>
                                                    <td>
                                                        {{ __("Stores the default language of the application or the user's choice.") }}
                                                    </td>
                                                    <td>
                                                        {{ __('Session') }}
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="w[2rem]">
                                                        {{ 'theme' }}
                                                    </td>
                                                    <td>
                                                        {{ __('Own') }}
                                                    </td>
                                                    <td>
                                                        {{ __("Stores the default theme of the application or the user's choice.") }}
                                                    </td>
                                                    <td>
                                                        {{ __('Session') }}
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="flex w-full text-sm py-3 justify-end gap-6">
                                        <a href="{{route('consent')}}" type="button" class="btn flex justify-center p-3 rounded-xl mx-auto mt-2 w-full">
                                            {{ __('Accept') }}
                                        </a>
                                        <a href="{{route('refuse')}}" type="button" class="btn flex justify-center p-3 rounded-xl mx-auto mt-2 w-full">
                                            {{ __('Refuse') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <a href="{{route('refuse-banner')}}" type="button" @click="showBanner = false" class="btn justify-center rounded text-center mt-2 w-full sm:mx-3">
                    {{ __('Refuse') }}
                </a>
            </div>
        </div>
    @elseif (session('consent') === 'refused')
        <a href="{{route('refuse')}}" class="sm:btn sm:pt-3.5 sm:pl-3 fixed sm:w-48 sm:h-12 sm:bottom-8 sm:text-sm sm:rounded-l-none sm:rounded-xl btn-cookies">
            {{ __('Manage consent') }}
        </a>
    @else
        <a href="{{route('refuse')}}" class="sm:btn sm:pt-3.5 sm:pl-3 fixed sm:w-48 sm:h-12 sm:bottom-8 sm:text-sm sm:rounded-l-none sm:rounded-xl btn-cookies">
            {{ __('Refuse consent') }}
        </a>
    @endif
</section>
