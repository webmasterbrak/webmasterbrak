@extends('components.layouts.app')

@section('title', __('Legal disclaimer'))

@section('content')
    <main class="bg-main">
        <div class="content">
            <div class="text-primary-200 dark:text-primary-300">
                {{ Breadcrumbs::render('legal') }}
            </div>
            <div class="flex-row justify-center">
                <h1 class="h1 ml-2 mt-1">
                    {{ __('Legal disclaimer') }}
                </h1>
                <section>
                    <h2 class="h2 my-8">
                        {{ __('Identification and ownership') }}
                    </h2>
                    <p class="text mb-12">
                        {{ __('In compliance with Article 10 of Law 34/2002, of July 11, 2002, of Information Society Services and Electronic Commerce (LSSICE or LSSI), the owner exposes its identification data:') }}
                    </p>
                    <ul class="text">
                        <li>
                            <p class="font-extrabold inline-flex mr-1 mb-3">
                                {{ __('Ownership:') }}
                            </p>
                            {{'Iván López Ordorica.'}}
                        </li>
                        <li>
                            <p class="font-extrabold inline-flex mr-1 mb-3">
                                {{ __('NIF:') }}
                            </p>
                            {{'50202716H'}}
                        </li>
                        <li>
                            <p class="font-extrabold inline-flex mr-1 mb-3">
                                {{ __('Address:') }}
                            </p>
                            {{'Rua dos Gorrions, 12 - 15172 A Coruña - España.'}}
                        </li>
                        <li>
                            <p class="font-extrabold inline-flex mr-1 mb-3">
                                {{ __('E-mail:') }}
                            </p>{{'ivan@webmasterbrak.dev'}}
                        </li>
                        <li>
                            <p class="font-extrabold inline-flex mr-1 mb-3">
                                {{ __('Web site:') }}
                            </p>
                            <a href="https://webmasterbrak.dev"
                               class="link underline"
                               target="_blank">
                                {{'https://webmasterbrak.dev'}}
                            </a>
                        </li>
                    </ul>
                </section>
            </div>
        </div>
    </main>
@endsection
