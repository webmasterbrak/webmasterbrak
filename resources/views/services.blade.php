@extends('components.layouts.app')

@section('title', __('Who am I?'))

@section('content')
    <main class="bg-main">
        <div class="content">
            <div class="text-primary-200 dark:text-primary-300">
                {{ Breadcrumbs::render('services') }}
            </div>
            <div class="flex justify-center h1">
                <div class="flex-none">
                    <i class="fa-solid fa-code text-xl"></i>
                </div>
                <h1 class="h1 ml-2 mt-0.5">
                    {{ __('Services') }}
                </h1>
            </div>
            <section class="mt-9 mx-6" id="custom-development">
                <h2 class="h2 my-8">
                    {{ __('Development of customized web services') }}
                </h2>
                <div class="text">
                    <h3 class="h3 mb-3">
                        {{ __('Need a web service that stands out?') }}
                    </h3>
                    <p class="text my-6">
                        {{ __('Customized and scalable web solutions that will help you achieve your business goals. I use the latest technologies and best practices to create web services that are both aesthetically pleasing and functional and offer an exceptional user experience.') }}
                    </p>
                    <h3 class="h3 mb-6">
                        {{ __('What I offer?') }}
                    </h3>
                    <ul class="list-disc list-inside">
                        <li>
                            {{ __('Custom web development: Development of scalable web services tailored to your needs.') }}
                        </li>
                        <li>
                            {{ __('E-commerce: I implement complete and secure online stores.') }}
                        </li>
                        <li>
                            {{ __('Systems integration: Connect your web service with other tools and platforms.') }}
                        </li>
                        <li>
                            {{ __('SEO Optimization: I position your website in the main search engines.') }}
                        </li>
                        <li>
                            {{ __('Maintenance and support: I provide continuous technical assistance.') }}
                        </li>
                    </ul>
                    <h3 class="h3 my-6">
                        {{ __('Why choose me?') }}
                    </h3>
                    <ul class="list-disc list-inside">
                        <li>
                            {{ __('Experience: I have years of experience in web development.') }}
                        </li>
                        <li>
                            {{ __('Technology: I use the latest technologies to create high quality web services.') }}
                        </li>
                        <li>
                            {{ __('Customer oriented: I listen to you and work with you to achieve your goals.') }}
                        </li>
                    </ul>
                    <h3 class="h3 my-12">
                        {{ __('Transform your online presence today! Contact me to request a custom quote.') }}
                    </h3>
                </div>
            </section>

            <section class="mt-16 mx-6 pb-1" id="security-analysis">
                <h2 class="h2 my-8">
                    {{ __('Security analysis') }}
                </h2>
                <div class="text">
                    <h3 class="h3 my-6">
                        {{ __('Protect your investment and your reputation, contact me today!') }}
                    </h3>
                    <p class="text mb-6">
                        {{ __('Are you concerned about the security of your web service? The online security analysis service provides you with a comprehensive assessment of your digital infrastructure. Using state-of-the-art tools and ethical hacking techniques, I identify critical vulnerabilities that could be exploited by cybercriminals.') }}
                    </p>
                    <h3 class="h3 mb-6">
                        {{ __('What I offer?') }}
                    </h3>
                    <ul class="list-disc list-inside">
                        <li>
                            {{ __('Vulnerability scanning: I detect and classify the most common weaknesses in your web service.') }}
                        </li>
                        <li>
                            {{ __('Penetration testing: I simulate real attacks to identify weak points in your system.') }}
                        </li>
                        <li>
                            {{ __('Code analysis: I check your source code for bugs that may compromise security.') }}
                        </li>
                        <li>
                            {{ __('Secure server configuration: I optimize the configuration of your servers to minimize the risk of attacks.') }}
                        </li>
                        <li>
                            {{ __('Detailed reports: You will receive a complete report with the results of the analysis and personalized recommendations.') }}
                        </li>
                    </ul>
                    <h3 class="h3 my-6">
                        {{ __('Why choose me?') }}
                    </h3>
                    <ul class="list-disc list-inside">
                        <li>
                            {{ __('Security expert: I have extensive experience in the field of cybersecurity.') }}
                        </li>
                        <li>
                            {{ __('State-of-the-art tools: I use the most advanced tools to ensure accurate results.') }}
                        </li>
                        <li>
                            {{ __('Personalized service: I adapt the services to the specific needs of your business.') }}
                        </li>
                        <li>
                            {{ __('Confidentiality: I treat your information with the utmost confidentiality.') }}
                        </li>
                    </ul>
                    <h3 class="h3 my-12">
                        {{ __('Sleep easy knowing your website is protected. Trust my security practices to keep your business safe.') }}
                    </h3>
                </div>
            </section>
        </div>
    </main>
@endsection
