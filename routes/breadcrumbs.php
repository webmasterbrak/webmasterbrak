<?php // routes/breadcrumbs.php

// Note: Laravel will automatically resolve `Breadcrumbs::` without
// this import. This is nice for IDE syntax and refactoring.
use Diglactic\Breadcrumbs\Breadcrumbs;

// This import is also not required, and you could replace `BreadcrumbTrail $trail`
//  with `$trail`. This is nice for IDE type checking and completion.
use Diglactic\Breadcrumbs\Generator as BreadcrumbTrail;

// Home
Breadcrumbs::for('home', function (BreadcrumbTrail $trail) {
    $trail->push(__('Home'), route('home'));
});

// Services
Breadcrumbs::for('services', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push(__('Services'), route('services'));
});

// Portfolio
Breadcrumbs::for('portfolio', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push(__('Portfolio'), route('portfolio'));
});

// Skills
Breadcrumbs::for('skills', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push(__('Skills'), route('skills'));
});

// Studies
Breadcrumbs::for('studies', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push(__('Studies'), route('studies'));
});

// Legal
Breadcrumbs::for('legal', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push(__('Legal disclaimer'), route('legal'));
});

// Cookies
Breadcrumbs::for('cookies', function (BreadcrumbTrail $trail) {
    $trail->parent('home');
    $trail->push(__('Cookies policy'), route('cookies'));
});
