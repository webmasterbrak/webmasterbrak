@extends('components.layouts.app')

@section('title', __('Cookies policy'))

@section('content')
    <main class="bg-main">
        <div class="content px-6">
            <div class="text-primary-200 dark:text-primary-300">
                {{ Breadcrumbs::render('cookies') }}
            </div>
            <div class="flex-row justify-center">
                <h1 class="h1 mb-12 mt-1">
                    {{ __('Cookies policy') }}
                </h1>
                <section class="pb-12">
                    <h2 class="h2 mb-6">
                        {{ __('What are cookies?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __("In English, the term 'cookie' means cookie, but in the field of web browsing, a 'cookie' is something completely different. When you access our Web Site, a small amount of text is stored on your device's browser called a 'cookie'. This text contains various information about your browsing habits, preferences, content customizations, etc.") }}
                        </p>
                        <p class="mb-2">
                            {{ __("There are other technologies that work in a similar way and are also used to collect data about your browsing activity. We will call all of these technologies together 'cookies'.") }}
                        </p>
                        <p class="mb-2">
                            {{ __('The specific uses we make of these technologies are described in this document.') }}
                        </p>
                    </div>
                    <h2 class="h2 mb-6 mt-8">
                        {{ __('What are cookies used for on this website?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __('Cookies are an essential part of how the Website works. The main purpose of our cookies is to improve your browsing experience. For example, to remember your preferences (language, country, etc.) during navigation and on future visits. The information collected in cookies also allows us to improve the website, adapt it to your interests as a user, speed up the searches you perform, etc.') }}
                        </p>
                        <p class="mb-2">
                            {{ __('In certain cases, if we have obtained your prior informed consent, we may use cookies for other uses, such as to obtain information that allows us to show you advertising based on the analysis of your browsing habits.') }}
                        </p>
                    </div>
                    <h2 class="h2 mb-6 mt-8">
                        {{ __('What are cookies NOT used for on this website?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __('No sensitive personally identifiable information such as your name, address, password, etc. is stored in the cookies we use.') }}
                        </p>
                    </div>
                    <h2 class="h2 mb-6 mt-8">
                        {{ __('Who uses the information stored in cookies?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __("The information stored in the cookies of our Website is used exclusively by us, except for those identified below as 'third party cookies', which are used and managed by external entities that provide us with services that improve the user experience. For example, the statistics collected on the number of visits, the most liked content, etc. are usually managed by Google Analytics.") }}
                        </p>
                    </div>
                    <h2 class="h2 mb-6 mt-8">
                        {{ __('How can you prevent the use of cookies on this Website?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __('If you prefer to avoid the use of cookies, you can REJECT their use, or you can CONFIGURE the ones you want to avoid and the ones you are allowed to use (in this document we give you more information about each type of cookie, its purpose, recipient, temporality, etc.).') }}
                        </p>
                        <p class="mb-2">
                            {{ __('If you have accepted them, we will not ask you again unless you delete the cookies on your device as indicated in the following section. If you want to revoke consent you will need to delete the cookies and reset them.') }}
                        </p>
                    </div>
                    <h2 class="h2 mb-6 mt-8">
                        {{ __('How do I disable and delete the use of cookies?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __('The Owner displays information about its Cookie Policy in the cookie banner accessible on all pages of the Website. The cookie banner displays essential information about data processing and allows the User to perform the following actions:') }}
                        </p>
                        <ul class="list-disc ml-3 mt-6">
                            <li>
                                <p class="inline-flex mr-1 mb-2">
                                    {{ __('ACCEPT or REFUSE the installation of cookies, or withdraw the consent previously granted.') }}
                                </p>
                            </li>
                            <li>
                                <p class="inline-flex mr-1 mb-2">
                                    {{ __('Change cookie preferences from the customize cookies page, which you can access from the cookie notice or from the customize cookies page.') }}
                                </p>
                            </li>
                            <li>
                                <p class="inline-flex mr-1 mb-2">
                                    {{ __('Get additional information on the cookie policy page.') }}
                                </p>
                            </li>
                        </ul>
                        <p class="mb-2">
                            {{ __('To restrict, block or delete cookies from this website (and those used by third parties) you can do so, at any time, by modifying your browser settings. Please note that these settings are different for each browser.') }}
                        </p>
                        <p class="mb-2">
                            {{ __('In the following links, you will find instructions on how to enable or disable cookies in the most common browsers:') }}
                        </p>
                        <ul class="list-disc ml-3 mt-6">
                            <li>
                                <a href="https://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-sitios-web-rastrear-preferencias?redirectslug=habilitar-y-deshabilitar-cookies-que-los-sitios-we&redirectlocale=es"
                                   target="_blank" class="inline-flex mr-1 mb-2 link underline">
                                    {{'Firefox'}}
                                </a>
                            </li>
                            <li>
                                <a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank"
                                   class="inline-flex mr-1 mb-2 link underline">
                                    {{'Google Chrome'}}
                                </a>
                            </li>
                            <li>
                                <a href="https://support.microsoft.com/es-es/windows/eliminar-y-administrar-cookies-168dab11-0753-043d-7c16-ede5947fc64d#ie=%22ie-10"
                                   target="_blank" class="inline-flex mr-1 mb-2 link underline">
                                    {{'Internet Explorer'}}
                                </a>
                            </li>
                            <li>
                                <a href="https://support.microsoft.com/es-es/windows/microsoft-edge-datos-de-exploraci%C3%B3n-y-privacidad-bb8174ba-9d73-dcf2-9b4a-c582b4e640dd"
                                   target="_blank" class="inline-flex mr-1 mb-2 link underline">
                                    {{'Microsoft Edge'}}
                                </a>
                            </li>
                            <li>
                                <a href="https://support.apple.com/es-es/105082" target="_blank"
                                   class="inline-flex mr-1 mb-2 link underline">
                                    {{'Safari'}}
                                </a>
                            </li>
                        </ul>
                    </div>
                    <h2 class="h2 mb-6 mt-8">
                        {{ __('What types of cookies are used on this website?') }}
                    </h2>
                    <div class="text">
                        <p class="mb-2">
                            {{ __('Each website uses its own cookies, on our website, we use those listed below:') }}
                        </p>
                        <h4 class="text-stone-800 dark:text-stone-400 mb-2 mt-6 uppercase font-extrabold">
                            {{ __('According to the entity that manages it') }}
                        </h4>
                        <p class="mb-2 font-extrabold">{{ __('Own cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __("They are those that are sent to the User's terminal equipment from a computer or domain managed by the editor itself and from which the service requested by the User is provided.") }}
                        </p>
                        <p class="mb-2 font-extrabold">
                            {{ __('Third-party cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __("Cookies are those that are sent to the User's terminal equipment from a computer or domain that is not managed by the editor, but by another entity that processes the data obtained through the cookies.") }}
                        </p>
                        <p class="mb-2">
                            {{ __('In the event that the cookies are served from a computer or domain managed by the editor itself, but the information collected through this is managed by a third party, they cannot be considered as own cookies if the third party uses them for its own purposes (for example, the improvement of the services it provides or the provision of advertising services in favor of other entities).') }}
                        </p>
                        <h4 class="text-stone-800 dark:text-stone-400 mb-2 mt-6 uppercase font-extrabold">
                            {{ __('According to purpose') }}
                        </h4>
                        <p class="mb-2 font-extrabold">
                            {{ __('Technical cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __('They are those necessary for navigation and the proper functioning of our website, such as controlling traffic and data communication, identifying the session, accessing parts of restricted access, making the application for registration or participation in an event, counting visits for billing purposes of software licenses with which the website service works, using security features during navigation, storing content for broadcasting videos or sound, enabling dynamic content (for example, animation of loading a text or image) or sharing content through social networks.') }}
                        </p>
                        <p class="mb-2 font-extrabold">
                            {{ __('Analytics cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __('They allow us to quantify the number of users and thus perform the measurement and statistical analysis of the use made by users of the website.') }}
                        </p>
                        <p class="mb-2 font-extrabold">
                            {{ __('Preference or personalization cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __('They are those that allow remembering information for the User to access the service with certain characteristics that may differentiate his experience from that of other users, such as, for example, the language, the number of results to be displayed when the User performs a search, the appearance or content of the service depending on the type of browser through which the User accesses the service or the region from which he accesses the service, etc.') }}
                        </p>
                        <p class="mb-2 font-extrabold">
                            {{ __('Behavioral advertising:') }}
                        </p>
                        <p class="mb-2">
                            {{ __('They are those that, processed by us or by third parties, allow us to analyze your browsing habits on the Internet so that we can show you advertising related to your browsing profile.') }}
                        </p>
                        <h4 class="text-stone-800 dark:text-stone-400 mb-2 mt-6 uppercase font-extrabold">
                            {{ __('Depending on the length of time, they remain active') }}
                        </h4>
                        <p class="mb-2 font-extrabold">
                            {{ __('Session cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __('They are those designed to collect and store data while the user accesses a web page.') }}
                        </p>
                        <p class="mb-2">
                            {{ __('They are usually used to store information that only needs to be kept for the provision of the service requested by the User on a single occasion (for example, a list of products purchased) and disappear at the end of the session.') }}
                        </p>
                        <p class="mb-2 font-extrabold">
                            {{ __('Persistent cookies:') }}
                        </p>
                        <p class="mb-2">
                            {{ __('These are cookies in which the data remain stored in the terminal and can be accessed and processed for a period defined by the cookie manager, which can range from a few minutes to several years. In this regard, it should be specifically assessed whether the use of persistent cookies is necessary, since the risks to privacy could be reduced by using session cookies. In any case, when persistent cookies are installed, it is recommended to reduce their temporary duration to the minimum necessary, taking into account the purpose of their use. To this effect, WG29 Opinion 4/2012 indicated that for a cookie to be exempt from the duty of informed consent, its expiration must be related to its purpose. As a result, session cookies are much more likely to be considered exempt than persistent cookies.') }}
                        </p>
                        <p class="mb-2 font-extrabold">
                            {{ __('Detail of cookies used on this website:') }}
                        </p>
                        <div class="flex inline text-lg">
                            <div class="w-36">
                                {{ '_token' }}
                            </div>
                            <div class="w-36">
                                {{ __('Own') }}
                            </div>
                            <div class="w-36">
                                {{ __('Technical') }}
                            </div>
                            <div>
                                {{ __('Session') }}
                            </div>
                        </div>
                        <div class="flex inline text-lg">
                            <div class="w-36">
                                {{ '_previous' }}
                            </div>
                            <div class="w-36">
                                {{ __('Own') }}
                            </div>
                            <div class="w-36">
                                {{ __('Technical') }}
                            </div>
                            <div>
                                {{ __('Session') }}
                            </div>
                        </div>
                        <div class="flex inline text-lg">
                            <div class="w-36">
                                {{ '_flash' }}
                            </div>
                            <div class="w-36">
                                {{ __('Own') }}
                            </div>
                            <div class="w-36">
                                {{ __('Technical') }}
                            </div>
                            <div>
                                {{ __('Session') }}
                            </div>
                        </div>
                        <div class="flex inline text-lg">
                            <div class="w-36">
                                {{ 'applocale' }}
                            </div>
                            <div class="w-36">
                                {{ __('Own') }}
                            </div>
                            <div class="w-36">
                                {{ __('Technical') }}
                            </div>
                            <div>
                                {{ __('Session') }}
                            </div>
                        </div>
                        <div class="flex inline text-lg">
                            <div class="w-36">
                                {{ 'theme' }}
                            </div>
                            <div class="w-36">
                                {{ __('Own') }}
                            </div>
                            <div class="w-36">
                                {{ __('Technical') }}
                            </div>
                            <div>
                                {{ __('Session') }}
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </main>
@endsection
