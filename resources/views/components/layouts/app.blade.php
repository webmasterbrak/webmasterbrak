<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="{{ session('theme') === 'light' ? 'light' : 'dark' }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>@yield('title') | {{config('app.name')}}</title>
        {!! SEO::generate() !!}
        @vite('resources/css/app.css')
        @vite('resources/js/app.js')
        <script src="https://kit.fontawesome.com/447636fad2.js" crossorigin="anonymous"></script>
    </head>
    <body>
        <style>
            [x-cloak] { display: none }
        </style>
        @include('partials.navbar')

        @include('partials.flash-session')

        @yield('content')

        @include('partials.footer')

        @include('partials.cookie-banner')
    </body>
    @stack('scripts')
</html>
