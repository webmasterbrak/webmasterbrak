<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{
    public function index()
    {
        SEOTools::setDescription(__('Portfolio of Iván López Ordorica (webmasterbrak).'));
        SEOTools::setCanonical(url('/'));
        SEOMeta::addKeyword(['Web development', 'Programming', 'Laravel', 'Php', 'Symfony']);
        SEOTools::opengraph()->setTitle(__('Home'));
        SEOTools::opengraph()->setUrl(url('/'));
        SEOTools::opengraph()->addProperty('type', 'pages');
        SEOTools::twitter()->setSite('@WebmasterBrak');
        SEOTools::twitter()->setTitle('Homepage');

        $projects = Cache::remember('projects', 3, function ()
        {
            return DB::table('projects')->where('status', 'production')->get()->take(2);
        });

        return view('index', compact('projects'));
    }

    public function services()
    {
        SEOTools::setDescription(__('About to Iván López Ordorica (webmasterbrak).'));
        SEOTools::setCanonical(url('/who-am-i'));
        SEOMeta::addKeyword(['Web development', 'Programming', 'Laravel', 'Php', 'Symfony', 'About']);
        SEOTools::opengraph()->setTitle(__('Who am I?'));
        SEOTools::opengraph()->setUrl(url('/who-am-i'));
        SEOTools::opengraph()->addProperty('type', 'pages');
        SEOTools::twitter()->setSite('@WebmasterBrak');
        SEOTools::twitter()->setTitle('Aboutpage');

        return view('services');
    }

    public function portfolio()
    {
        SEOTools::setDescription(__('Projects developed by Iván López Ordorica (webmasterbrak).'));
        SEOTools::setCanonical(url('/projects'));
        SEOMeta::addKeyword(['Web development', 'Programming', 'Laravel', 'Php', 'Symfony', 'Projects']);
        SEOTools::opengraph()->setTitle(__('Projects'));
        SEOTools::opengraph()->setUrl(url('/projects'));
        SEOTools::opengraph()->addProperty('type', 'pages');
        SEOTools::twitter()->setSite('@WebmasterBrak');
        SEOTools::twitter()->setTitle('Projectspage');

        $projects = Cache::remember('projects', 3, function ()
        {
            return DB::table('projects')->get();
        });

        return view('portfolio', compact('projects'));
    }

    public function skills()
    {
        SEOTools::setDescription(__('Skills to Iván López Ordorica (webmasterbrak).'));
        SEOTools::setCanonical(url('/skills'));
        SEOMeta::addKeyword(['Web development', 'Programming', 'Laravel', 'Php', 'Symfony', 'Skills']);
        SEOTools::opengraph()->setTitle(__('Skills'));
        SEOTools::opengraph()->setUrl(url('/skills'));
        SEOTools::opengraph()->addProperty('type', 'pages');
        SEOTools::twitter()->setSite('@WebmasterBrak');
        SEOTools::twitter()->setTitle('Skillspage');

        return view('skills');
    }

    public function studies()
    {
        SEOTools::setDescription(__('Studies to Iván López Ordorica (webmasterbrak).'));
        SEOTools::setCanonical(url('/studying'));
        SEOMeta::addKeyword(['Web development', 'Programming', 'Laravel', 'Php', 'Symfony', 'Studying']);
        SEOTools::opengraph()->setTitle(__('Studying'));
        SEOTools::opengraph()->setUrl(url('/studying'));
        SEOTools::opengraph()->addProperty('type', 'pages');
        SEOTools::twitter()->setSite('@WebmasterBrak');
        SEOTools::twitter()->setTitle('Studyingpage');

        return view('studies');
    }
}
