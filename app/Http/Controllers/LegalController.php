<?php

namespace App\Http\Controllers;

use Artesaos\SEOTools\Facades\SEOMeta;

class LegalController extends Controller
{
    public function disclaimer()
    {
        SEOMeta::addMeta('robots', 'none');

        return view('legal.legal');
    }

    public function cookies()
    {
        SEOMeta::addMeta('robots', 'none');

        return view('legal.cookies');
    }
}
