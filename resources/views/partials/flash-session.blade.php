@if($message = Session::get('success'))
    <div x-data="{ flashMessage: false }"
         x-init="() => {setTimeout(() => flashMessage = true, 500);setTimeout(() => flashMessage = false, 5000);}"
         x-show="flashMessage"
         @click.away="flashMessage = false"
         x-cloak
         class="text-primary-50 dark:text-stone-950 bg-primary-600 dark:bg-primary-600 px-6 py-4 border-0">
        <span class="text-xl inline-block mr-5 align-middle">
            <i class="fa-solid fa-check"></i>
        </span>
        <span class="inline-block align-middle mr-8">
            {{$message}}
        </span>
    </div>
@endif
@if($message = Session::get('error'))
    <div x-data="{ flashMessage: false }"
         x-init="() => {setTimeout(() => flashMessage = true, 500);setTimeout(() => flashMessage = false, 5000);}"
         x-show="flashMessage"
         @click.away="flashMessage = false"
         x-cloak
         class="text-secondary-50 dark:text-secondary-900 bg-error dark:bg-dark-error px-6 py-4 border-0 rounded relative mb-4 mx-6">
    <span class="text-xl inline-block mr-5 align-middle">
        <i class="fa-solid fa-exclamation"></i>
    </span>
        <span class="inline-block align-middle mr-8">
        {{$message}}
    </span>
    </div>
@endif
@if ($errors->any())
    <div x-data="{ flashMessage: false }"
         x-init="() => {setTimeout(() => flashMessage = true, 500);setTimeout(() => flashMessage = false, 5000);}"
         x-show="flashMessage"
         @click.away="flashMessage = false"
         x-cloak
         class="text-secondary-50 dark:text-secondary-900 bg-error dark:bg-dark-error px-6 py-4 border-0 rounded relative mb-4 mx-6">
        <ul>
            @foreach ($errors->all() as $error)
                <span class="text-xl inline-block mr-5 align-middle">
            <i class="fa-solid fa-exclamation"></i>
        </span>
                <li class="inline-block align-middle mr-8">
                    {{$error}}
                </li>
            @endforeach
        </ul>
    </div>
@endif
