<div class="flex items-end w-full bg-primary-950 dark:bg-stone-950">
    <footer class="w-full body-font">
        <section>
            <div class="flex flex-col flex-wrap px-5 py-6 mx-auto md:items-center lg:items-start md:flex-row md:flex-no-wrap">
                <div class="flex-shrink-0 w-64 mx-auto text-center md:mb-6">
                    <p class="text-stone-200 dark:text-stone-300 text-xl mt-2">
                        {{ __('Design, code and ship!') }}
                    </p>
                    <div class="mt-6">
                    <span class="inline-flex justify-center mt-2 sm:ml-auto sm:mt-0 sm:justify-start">
                        <a href="https://gitlab.com/webmasterbrak" target="_blank">
                            <img class="w-6 h-6" src="{{asset('storage/svg/Gitlab.svg')}}" alt="gitlab">
                        </a>
                        <a href="https://www.linkedin.com/in/iv%C3%A1n-l%C3%B3pez-ordorica/" target="_blank" class="ml-3">
                            <img class="w-6 h-6" src="{{asset('storage/svg/Linkedin.svg')}}" alt="linkedin">
                        </a>
                        <a href="https://twitter.com/webmasterbrak" target="_blank" class="ml-3">
                            <img class="w-6 h-6" src="{{asset('storage/svg/X.svg')}}" alt="x">
                        </a>
                        <a href="https://github.com/wbrak" target="_blank" class="ml-3">
                            <img class="w-7 h-7" src="{{asset('storage/svg/Github.svg')}}" alt="github">
                        </a>
                    </span>
                    </div>
                </div>
                <div class="flex flex-wrap flex-grow mt-10 -mb-10 text-center md:mt-0">
                    <div class="w-full px-4 lg:w-1/4 md:w-1/2">
                        <h2 class="title-footer">
                            {{ __('Main') }}
                        </h2>
                        <nav class="mb-6 list-none">
                            <ul>
                                <li>
                                    <a href="{{ route('home') }}" class="{{ (Route::is('home')) ? 'active' : 'link' }}">
                                        {{ __('Home') }}
                                    </a>
                                </li>
                                <li class="mt-2">
                                    <a href="{{ route('services') }}" class="{{ (Route::is('services')) ? 'active' : 'link' }}">
                                        {{ __('Services') }}
                                    </a>
                                </li>
                                <li class="mt-2">
                                    <a href="{{ route('portfolio') }}" class="{{ (Route::is('projects')) ? 'active' : 'link' }}">
                                        {{ __('Portfolio') }}
                                    </a>
                                </li>
                                <li class="mt-2">
                                    <a href="{{ route('skills') }}" class="{{ (Route::is('skills')) ? 'active' : 'link' }}">
                                        {{ __('Skills') }}
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="w-full px-4 lg:w-1/4 md:w-1/2">
                        <h2 class="title-footer">
                            {{ __('Studies') }}
                        </h2>
                        <nav class="mb-6 list-none">
                            <ul>
                                <li class="mt-2">
                                    <a href="{{route('studies')}}" class="{{ (Route::is('studies')) ? 'active' : 'link' }}">
                                        {{ __('Studies') }}
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="w-full px-4 lg:w-1/4 md:w-1/2">
                        <h2 class="title-footer">
                            {{ __('Legal') }}
                        </h2>
                        <nav class="mb-6 list-none">
                            <ul>
                                <li>
                                    <a href="{{ route('legal') }}" class="{{ (Route::is('legal')) ? 'active' : 'link' }}">
                                        {{ __('Legal disclaimer') }}
                                    </a>
                                </li>
                                <li class="mt-2">
                                    <a href="{{ route('cookies') }}" class="{{ (Route::is('cookies')) ? 'active' : 'link' }}">
                                        {{ __('Cookies policy') }}
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                    <div class="w-full px-4 lg:w-1/4 md:w-1/2">
                        <h2 class="title-footer">
                            {{ __('Contact') }}
                        </h2>
                        <div class="mb-6 list-none">
                            <ul>
                                <li class="link mb-2 underline">
                                    <?php echo "<a href='mailto:" . 'ivan@webmasterbrak.dev' . "'>";?>{{ 'ivan@webmasterbrak.dev' }}
                                </li>
                                <li class="link mb-2 underline">
                                    <?php echo "<a href='mailto:" . 'webmasterbrak@gmail.com' . "'>";?>{{ 'webmasterbrak@gmail.com' }}
                                </li>
                                <div class="flex justify-center mt-2">
                                    <a class="inline-flex link underline"
                                       href="https://wa.me/34687564035?text=Estoy%20interesado%20en%20tus%20servicios"
                                       target="_blank">
                                        <img class="w-6 h-6" src="{{asset('storage/svg/Whatsapp.svg')}}" alt="whatsapp">
                                        <p class="ml-2">
                                            {{ __('Send WhatsApp') }}
                                        </p>
                                    </a>
                                </div>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="bg-primary-900 dark:bg-primary-800 py-1">
                <p class="text text-center text-xs md:text-base">
                    {{'© '}}<?php echo date('Y') ?>
                    <a href="https://webmasterbrak.dev" target="_blank" class="link capitalize -mr-0.5">
                        {{' webmasterbrak'}}
                    </a>. {{ __('All rigths reserved.') }} {{ __('Powered by') }}
                    <a href="https://alojatuweb.com" target="_blank" class="link capitalize -mr-0.5">
                        {{'alojaTuWeb | '}}{{ __('Integral web services') }}
                    </a>.
                </p>
            </div>
        </section>
    </footer>
</div>
