@extends('components.layouts.app')

@section('title', __('Studies'))

@section('content')
    <main class="bg-main">
        <section class="content">
            <div class="text-primary-200 dark:text-primary-300">
                {{ Breadcrumbs::render('studies') }}
            </div>
            <div class="flex justify-center h1">
                <div class="flex-none">
                    <i class="fa-solid fa-graduation-cap text-lg"></i>
                </div>
                <h1 class="h1 ml-2 mt-1">
                    {{ __('Studies') }}
                </h1>
            </div>
            <div class="flex flex-col items-center md:grid md:grid-cols-2 gap-12 mt-20">
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('Intermediate Level Training Cycle') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('IES Fernando Wirtz Suárez') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Microcomputer Systems and Network Technician.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Certificado-FP.jpg')}}" alt="Certificate FP" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('Cybersecurity Professional Certificate') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Google') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Google Certified in security analysis.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Ciberseguridad.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('Artificial intelligence and productivity') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Google') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Google Certified in Artificial intelligence.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/IASantander.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('Master in PHP') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('PHP, SQL, POO, MVC, Laravel, Symphony, WordPress and +.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Master-php.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('Master in JavaScript') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('JavaScript, jQuery, JSON, Angular, NodeJS, MEAN.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Master-javascript.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('Master in CSS') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Responsive, SASS, Flexbox, Grid y Bootstrap.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Master-css.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('CentOS7') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Everything you need to master it.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Centos7.jpg')}}" alt="Certificate CentOS7" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('PHP and MySQLi, basic concepts') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('PHP and MySQLi, basic concepts for beginners.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Php-mysqli.jpg')}}" alt="Certificate PHP and MySQLi" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('PrestaShop Modules') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('PrestaShop module development.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Modulos-prestashop.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div>
                    <h3 class="h3 mb-3">
                        {{ __('WooCommerce') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Online store with WooCommerce from scratch.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Woocommerce.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
                <div class="mb-6">
                    <h3 class="h3 mb-3">
                        {{ __('Programming logic') }}
                    </h3>
                    <p class="mb-3 text text-lg">
                        {{ __('Udemy') }}
                    </p>
                    <p class="mb-9 text text-lg">
                        {{ __('Learn to program in any language.') }}
                    </p>
                    <div class="certificate">
                        <img src="{{asset('storage/certificates/Logica-programacion.jpg')}}" alt="Image certificate" />
                    </div>
                </div>
            </div>
        </section>
    </main>
@endsection
