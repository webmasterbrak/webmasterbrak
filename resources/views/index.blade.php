@extends('components.layouts.app')

@section('title', __('Home'))

@section('content')
    <main class="bg-main">
        <section class="content">
            <div class="flex justify-center animate-pulse text-primary-400 text-4xl py-8">
                <div class="flex-none">
                    <i class="fa-solid fa-code"></i>
                </div>
                <h1 class="ml-2">
                    {{config('app.name')}}
                </h1>
            </div>
            <div class="text-center mx-6 mt-2 font-mono">
                <div class="flex justify-center">
                    <p class="text-secondary-300">
                        {{ __("I'm not an AI. I am a fullstack developer and webmaster with experience helping companies to have online visualization.") }}
                    </p>
                </div>
                <p class="text-info">
                    {{ __('My greatest strength is attention to detail and driving efficiency and I have a talent for learning new things.') }}
                </p>
                <p class="text-stone-400">
                    {{ __('I am passionate about software architecture, cybersecurity and everything related to the IT world.') }}
                </p>
                <p class="text-slate">
                    {{ __('Most importantly, I value having fun and being collaborative.') }}
                </p>
            </div>
            <div class="flex justify-center h2 mt-12">
                <div class="flex-none">
                    <i class="fa-solid fa-circle-user text-xl"></i>
                </div>
                <div>
                    <h2 class="ml-2 -mt-0.3">
                        {{ __('Who am I?') }}
                    </h2>
                </div>
            </div>
            <div class="text-rose mt-6 mx-6 font-mono">
                <p class="mb-2">
                    {{ __('Hello, my name is Iván López Ordorica (nickname/alias webmasterbrak) and I have been a web developer for many years, so many that my first internet connection was with a 512 kB modem and the first web I developed was pure html and css, needless to say that the almighty WordPress (2003) did not exist yet.') }}
                </p>
                <p class="mb-2">
                    {{ __('I am co-founder, web developer and system administrator at AlojaTuWeb since 2002, helping companies in their efforts in digital transformation and online visualization. Immersed in the refactoring of this application to bring it to hexagonal architecture with DDD under symfony7.') }}
                </p>
            </div>
            <div class="flex justify-center mt-6">
                <button class="btn">
                    <a href="{{route('curriculum')}}" target="_blank">
                        {{ __('Download CV') }}
                    </a>
                </button>
            </div>
        </section>
        <section>
            <div class="flex-row text-center mt-12 pb-3">
                <div class="inline-flex h2">
                    <i class="fa-solid fa-person-digging text-lg"></i>
                    <h2 class="ml-1 -mt-3">
                        {{ __('Services') }}
                    </h2>
                </div>
                <p class="text max-w-[20rem] sm:max-w-[36rem] md:max-w-[48rem] lg:max-w-[63rem] mx-auto mt-3 mb-0.5">
                    {{ __('I develop custom websites that reflect your brand and objectives. Online security scans detect vulnerabilities and help you prevent cyber attacks.') }}
                </p>
                <a href="{{route('services')}}">
                    <button class="learn-more">
                        {{ __('Learn more') }}
                        <i class="fa-solid fa-arrow-right mt-0.5"></i>
                    </button>
                </a>
                <div class="flex flex-col sm:flex-row max-w-[20rem] sm:max-w-[36rem] md:max-w-[48rem] lg:max-w-[63rem] mx-auto my-12 gap-12 justify-center">
                    <a href="{{url('services#custom-development')}}" class="btn w-full h-24">
                        <button class="mt-6">
                            {{ __('Customized development') }}
                        </button>
                    </a>
                    <a href="{{url('services#security-analysis')}}" class="btn w-full h-24">
                        <button class="mt-6">
                            {{ __('Security analysis') }}
                        </button>
                    </a>
                </div>
            </div>
        </section>
        <section>
            <div class="flex-row text-center mt-6">
                <div class="inline-flex h2">
                    <i class="fa-solid fa-folder-tree text-lg"></i>
                    <h2 class="ml-2 -mt-3">
                        {{ __('Portfolio') }}
                    </h2>
                </div>
                <p class="text max-w-[20rem] sm:max-w-[36rem] md:max-w-[48rem] lg:max-w-[63rem] mx-auto mt-3 mb-0.5">
                    {{ __('Discover how I build innovative digital experiences. My portfolio presents a selection of projects ranging from static websites to interactive web applications.') }}
                </p>
                <a href="{{route('portfolio')}}">
                    <button class="learn-more">
                        {{ __('Learn more') }}
                        <i class="fa-solid fa-arrow-right mt-0.5"></i>
                    </button>
                </a>
            </div>
        </section>
        <section>
            @foreach($projects as $project)
                <div class="card">
                    <div class="relative rounded-r-none rounded-xl sm:shrink-0">
                        <img class="sm:w-72 sm:h-full object-cover" src="{{asset($project->image)}}" alt="project-image"/>
                    </div>
                    <div class="p-3">
                        <div class="flex">
                            <div class="block mb-4 font-semibold uppercase">
                                {{ __('In') }} {{ __($project->status) }}
                            </div>
                        </div>
                        <h4 class="block mb-2 text-2xl font-semibold">
                            {{ __($project->title) }}
                        </h4>
                        <p class="block mb-8">
                            {{ __($project->description) }}
                        </p>
                        <div class="flex">
                            <div class="block mb-2">
                                {{ __('Technologies') }}:
                            </div>
                            <p class="ml-1 uppercase">
                                {{ __($project->technologies) }}
                            </p>
                        </div>
                        <div class="flex">
                            <div class="block mb-2">
                                {{ 'Web:' }}
                            </div>
                            @if($project->link)
                                <a href="{{$project->link}}" target="_blank" class="ml-1 card-link">
                                    {{ __($project->link) }}
                                </a>
                            @else
                                <div class="ml-1">
                                    {{ __('Not available.') }}
                                </div>
                            @endif
                        </div>
                        <div class="flex">
                            <div class="block mb-2">
                                {{ 'Git:' }}
                            </div>
                            @if($project->git === 'Private')
                                <div class="ml-1">
                                    {{ __('Private.') }}
                                </div>
                            @elseif($project->git === null)
                                <div class="ml-1">
                                    {{ __('Not available.') }}
                                </div>
                            @else
                                <a href="{{$project->git}}" target="_blank" class="ml-1 card-link">
                                    {{ __($project->git) }}
                                </a>
                            @endif
                        </div>
                        <div class="flex">
                            <div class="block mb-2">
                                {{ __('Type') }}:
                            </div>
                            <p class="ml-1 capitalize">
                                {{ __($project->type) }}
                            </p>
                        </div>
                    </div>
                </div>
            @endforeach
        </section>
        <section>
            <div class="flex-row text-center mt-20">
                <div class="inline-flex h2">
                    <i class="fa-solid fa-screwdriver-wrench text-lg"></i>
                    <h2 class="ml-1 -mt-3">
                        {{ __('Skills') }}
                    </h2>
                </div>
                <p class="text max-w-[20rem] sm:max-w-[36rem] md:max-w-[48rem] lg:max-w-[63rem] mx-auto mt-3 mb-0.5">
                    {{ __('These are some of the skills that I put into practice whenever necessary, if you want to know the rest of the skills in their different levels, you can visit the skills page.') }}
                </p>
                <a href="{{route('skills')}}">
                    <button class="learn-more">
                        {{ __('Learn more') }}
                        <i class="fa-solid fa-arrow-right mt-0.5"></i>
                    </button>
                </a>
                <div class="sm:flex sm:flex-row max-w-[38rem] sm:max-w-[40rem] md:max-w-[48rem] lg:max-w-[63rem] text-stone-900 dark:text-stone-300 gap-6 mx-auto my-12">
                    <div class="w-80 sm:w-32 mx-auto mb-3">
                        <span class="text-center text-stone-950 dark:text-stone-300">
                            {{ 'PHP' }}
                        </span>
                        <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                    </div>
                    <div class="w-80 sm:w-32 mx-auto mb-3">
                        <span class="text-center text-stone-950 dark:text-stone-300">
                            {{ 'Laravel' }}
                        </span>
                        <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                    </div>
                    <div class="w-80 sm:w-32 mx-auto mb-3">
                        <span class="text-center text-stone-950 dark:text-stone-300">
                            {{ 'SQL' }}
                        </span>
                        <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                    </div>
                    <div class="w-80 sm:w-32 mx-auto mb-3">
                        <span class="text-center text-stone-950 dark:text-stone-300">
                            {{ 'JavaScript' }}
                        </span>
                        <span class="percentage-background-skill"><span class="percentage-skill w-2/5"></span></span>
                    </div>
                    <div class="w-80 sm:w-32 mx-auto mb-3">
                        <span class="text-center text-stone-950 dark:text-stone-300">
                            {{ 'Git' }}
                        </span>
                        <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                    </div>
                    <div class="w-80 sm:w-32 mx-auto mb-3">
                        <span class="text-center text-stone-950 dark:text-stone-300">
                            {{ 'Linux' }}
                        </span>
                        <span class="percentage-background-skill"><span class="percentage-skill w-3/4"></span></span>
                    </div>
                </div>
            </div>
        </section>
        <section>
            <div class="flex-row text-center mt-20 pb-12">
                <div class="inline-flex h2">
                    <i class="fa-solid fa-graduation-cap text-lg"></i>
                    <h2 class="ml-1 -mt-3">
                        {{ __('Studies') }}
                    </h2>
                </div>
                <p class="text max-w-[20rem] sm:max-w-[36rem] md:max-w-[48rem] lg:max-w-[63rem] mx-auto mt-3 mb-0.5">
                    {{ __('Here you can learn about the studies I have been doing over the years.') }}
                </p>
                <a href="{{route('studies')}}">
                    <button class="learn-more">
                        {{ __('Learn more') }}
                        <i class="fa-solid fa-arrow-right mt-0.5"></i>
                    </button>
                </a>
            </div>
        </section>
    </main>
@endsection
