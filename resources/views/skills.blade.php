@extends('components.layouts.app')

@section('title', __('Skills'))

@section('content')
    <main class="bg-main">
        <div class="content">
            <div class="text-primary-200 dark:text-primary-300">
                {{ Breadcrumbs::render('skills') }}
            </div>
            <div class="flex justify-center h1">
                <div class="flex-none">
                    <i class="fa-solid fa-screwdriver-wrench text-xl"></i>
                </div>
                <h1 class="h1 ml-2 mt-1">
                    {{ __('Skills') }}
                </h1>
            </div>
            <section class="mx-6 pb-12">
                <p class="text my-8">
                    {{ __('My frontend (HTML, CSS, JavaScript) and backend (PHP, SQL) web development skills, combined with my experience in frameworks such as Laravel and Angular, allow me to create intuitive user interfaces and robust web applications. In addition, my ability to work collaboratively and my focus on problem-solving allow me to adapt to changes and deliver successful projects.') }}
                </p>
                <h2 class="h2 my-9">
                    {{ __('Digital hard skills') }}
                </h2>
                <h3 class="h3 my-6">
                    {{ __('Programming languages') }}
                </h3>
                <div class="flex-row text-center mt-3">
                    <div class="section-skill">
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'PHP' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'C' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-2/5"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'JavaScript' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-2/5"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Python' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-1/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'SQL' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                    </div>
                </div>
                <h3 class="h3 my-6">
                    {{ __('Frameworks') }}
                </h3>
                <div class="flex-row text-center mt-3">
                    <div class="section-skill">
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Laravel' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Symfony' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-2/5"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Slim' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-2/5"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Angular' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-1/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Bootstrap-Css' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-3/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Tailwind-Css' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-3/4"></span></span>
                        </div>
                    </div>
                </div>
                <h3 class="h3 my-6">
                    {{ __('Varios') }}
                </h3>
                <div class="flex-row text-center mt-3">
                    <div class="section-skill">
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Git' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'PhpStorm' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Postman' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-2/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Linux' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-3/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ 'Bash' }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                    </div>
                </div>
                <h2 class="h2 my-12">
                    {{ __('Digital soft skills') }}
                </h2>
                <div class="flex-row text-center mt-3">
                    <div class="section-skill">
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ __('Teamwork') }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ __('Self-learning capacity') }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ __('Adaptability') }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-2/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ __('Troubleshooting') }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ __('Critical thinking') }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-3/4"></span></span>
                        </div>
                        <div class="w-80 sm:w-32 mx-auto mb-3">
                            <span class="text-center text-stone-950 dark:text-stone-300">
                                {{ __('Proactivity') }}
                            </span>
                            <span class="percentage-background-skill"><span class="percentage-skill w-4/6"></span></span>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </main>
@endsection
