<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Curriculum Webmasterbrak</title>
</head>
<body style="font-family:Arial,Helvetica,sans-serif;margin:0;padding:0;background:white;">
    <h1 style="margin-top:-1.5rem;margin-bottom:0.5rem;font-size:24px;text-align:center;">
        Iván López Ordorica
    </h1>
    <div style="height:2px;background-color:#477a02;"></div>
    <div style="text-align:center;margin:0.5rem;text-transform:uppercase;font-weight:bold;font-size:14px;">
        {{ __('Personal data') }}
    </div>
    <div style="margin-left:0.5rem;margin-right:0.5rem;">
        <ul style="font-size:14px;text-align:center;list-style-type:none;display:inline;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('E-mail:') }}</li>
            <li style="list-style-type:none;display:inline;">webmasterbrak@gmail.com</li>
            <li style="font-weight:bold;list-style-type:none;display:inline">{{ __('Address:') }}</li>
            <li style="list-style-type:none;display:inline;">Rua Gorrions, 12 - 15172, Perillo, España</li>
        </ul>
        <ul style="list-style-type:none;display:inline;margin-top:0.5rem;font-size:14px;text-align:center;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Telephone:') }}</li>
            <li style="list-style-type:none;display:inline;">+34 687564035</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Web site:') }}</li>
            <li style="list-style-type:none;display:inline;"><a href="https://webmasterbrak.dev">webmasterbrak.dev</a></li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">Linkedin:</li>
            <li style="list-style-type:none;display:inline;"><a href="https://www.linkedin.com/in/iv%C3%A1n-l%C3%B3pez-ordorica/">Iván López Ordorica</a></li>
        </ul>
    </div>
    <div style="height:2px;background-color:#477a02;margin-top:0.5rem;"></div>
    <div style="text-align:center;margin:0.5rem;text-transform:uppercase;font-weight:bold;font-size:14px;">
        {{ __('About me') }}
    </div>
    <p style="font-size:14px;margin-top:-0.5rem;">{{ __("I have been a web developer for many years, so many that my first internet connection was with a 512 kB modem and the first web I developed was pure HTML and CSS.") }}</p>
    <p style="font-size:14px;margin-top:-1rem">{{ __("My greatest strength is attention to detail and driving efficiency and I have a talent for learning new things.") }}</p>
    <p style="font-size:14px;margin-top:-1rem">{{ __("I am passionate about software architecture, cybersecurity and everything related to the IT world.") }}</p>
    <div style="height:2px;background-color:#477a02;"></div>
    <div style="text-align:center;margin:0.5rem;text-transform:uppercase;font-weight:bold;font-size:14px;">
        {{ __('Work experience') }}
    </div>
    <div style="font-size:14px;margin-top:-1rem;">
        <p style="text-align:center;margin-bottom:0.3rem;">[27/05/2024 – ]</p>
        <ul style="list-style-type:none;display:inline;margin-left:5rem;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Company:') }}</li>
            <li style="list-style-type:none;display:inline;">Group Saltó</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Modality:') }}</li>
            <li style="list-style-type:none;display:inline;">{{ __('Presential') }}</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('City:') }}</li>
            <li style="list-style-type:none;display:inline;">Galicia, Spain</li>
        </ul>
        <p style="margin-bottom:-1rem;margin-top:0.5rem;">{{ __('IT field technician for important companies such as CaixaBank, Santander, H&M, Primark or Eulen.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Incident resolution - Information technology management - Windows system administration') }}</p>
    </div>
    <div style="font-size:14px;margin-top:0.5rem;">
        <p style="text-align:center;margin-bottom:0.3rem;">[02/03/2023 – 15/04/2024]</p>
        <ul style="list-style-type:none;display:inline;margin-left:2rem;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Company:') }}</li>
            <li style="list-style-type:none;display:inline;">Multinacional Grupo Hotusa</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Modality:') }}</li>
            <li style="list-style-type:none;display:inline;">{{ __('Presential') }}</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('City:') }}</li>
            <li style="list-style-type:none;display:inline;">A Coruña, Spain</li>
        </ul>
        <p style="margin-bottom:-1rem;margin-top:0.5rem;">{{ __("Development and maintenance of applications for the internal management of the group's hotels.") }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Frontend developed in Html, Css and JavaScript with AngularJs version 1.5.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Backend developed with Php 5.4 (Phalcon 2.2), databases with MySQL 5.6 and MongoDb. Web Scraping with Python.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Management of the different company servers with Ubuntu 22.04.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __("Creation and maintenance of user accounts for the group's applications.") }}</p>
        <p style="margin-bottom:-1rem;">{{ __("Version management with git for the development and maintenance of the company's repositories.") }}</p>
    </div>
    <div style="font-size:14px;margin-top:0.5rem;">
        <p style="text-align:center;margin-bottom:0.3rem;">[06/10/2022 – 06/02/2023]</p>
        <ul style="list-style-type:none;display:inline;margin-left:4rem;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Company:') }}</li>
            <li style="list-style-type:none;display:inline;">Four Cats Media</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('City:') }}</li>
            <li style="list-style-type:none;display:inline;">Madrid, Spain</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Modality:') }}</li>
            <li style="list-style-type:none;display:inline;">{{ __('Remote') }}</li>
        </ul>
        <p style="margin-bottom:-1rem;margin-top:0.5rem;">{{ __('Development and maintenance of company websites, web pages, databases, marketing campaigns and servers.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Applications developed with Php 5.4 (Phalcon 3.4), MySQL 5.6, marketing with Klaviyo, servers with Ubuntu 20.04.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __("Version management with git for the company's repositories in bitbucket.") }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Organization of project tasks with Trello.') }}</p>
    </div>
    <div style="font-size:14px;margin-top:0.5rem;">
        <p style="text-align:center;margin-bottom:0.3rem;">[01/07/2022 – 01/09/2022]</p>
        <ul style="list-style-type:none;display:inline;margin-left:2rem;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Company:') }}</li>
            <li style="list-style-type:none;display:inline;">Colaboración Iberdrola</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('City:') }}</li>
            <li style="list-style-type:none;display:inline;">A Coruña, Spain</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Modality:') }}</li>
            <li style="list-style-type:none;display:inline;">{{ __('Remote') }}</li>
        </ul>
        <p style="margin-bottom:-1rem;margin-top:0.5rem;">{{ __("Collaboration with web consultancy A Coruña for the development of a new section in Iberdrola's website.") }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Developed with Angular 12. Framework configuration, HTML and CSS layout with bootstrap.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Project version management with git.') }}</p>
    </div>
    <div style="font-size:14px;margin-top:0.5rem;">
        <p style="text-align:center;margin-bottom:0.3rem;">[05/06/2021 – 05/06/2022]</p>
        <ul style="list-style-type:none;display:inline;margin-left:6rem;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Company:') }}</li>
            <li style="list-style-type:none;display:inline;">Inetum</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('City:') }}</li>
            <li style="list-style-type:none;display:inline;">A Coruña, Spain</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Modality:') }}</li>
            <li style="list-style-type:none;display:inline;">{{ __('Remote') }}</li>
        </ul>
        <p style="margin-bottom:-1rem;margin-top:0.5rem;">{{ __('Creation of Load Runner scripts in C language for testing.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Performance and load tests with Performance Center (Micro Focus).') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Review of Inditex e-commerce platforms and adaptation of scripts to test new flows and functionalities.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __("Version management with git and git-flow for the company's repositories in bitbucket.") }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Organization of project tasks with Jira and Confluence.') }}</p>
    </div>
    <div style="font-size:14px;margin-top:0.5rem;">
        <p style="text-align:center;margin-bottom:0.3rem;">[08/07/2020 – 04/03/2021]</p>
        <ul style="list-style-type:none;display:inline;margin-left:1rem;">
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Company:') }}</li>
            <li style="list-style-type:none;display:inline;">Dimensiona Consultoría</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('City:') }}</li>
            <li style="list-style-type:none;display:inline;">Santiago de Compostela</li>
            <li style="font-weight:bold;list-style-type:none;display:inline;">{{ __('Modality:') }}</li>
            <li style="list-style-type:none;display:inline;">{{ __('Remote') }}</li>
        </ul>
        <p style="margin-bottom:-1rem;margin-top:0.5rem;">{{ __('Development of an application for the training of employees of the subscribing companies.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __('Applications developed with Php 7 (Laravel), non-relational bd with MongoDb.') }}</p>
        <p style="margin-bottom:-1rem;">{{ __("Version management with git for the company's repositories.") }}</p>
    </div>
    <div style="height:2px;background-color:#477a02;margin-top:3rem;"></div>
    <div style="font-size:14px;margin-top:0.5rem;text-transform:uppercase;font-weight:bold;text-align:center;">
        {{ __('Studies') }}
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:-1rem;">{{ __('Intermediate Level Training Cycle') }}</p>
        <p style="margin-bottom:-0.3rem;">{{ __('IES Fernando Wirtz Suárez') }}</p>
        <p style="margin-bottom:-0.3rem;">{{ __('Microcomputer Systems and Network Technician') }}</p>
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:-1rem;margin-top:1.5rem;">{{ __('Cybersecurity') }} | <a href="https://www.coursera.org/programs/becas-google-fundae-sepe-ciberseguridad-61myy/professional-certificates/google-cybersecurity?collectionId=WosjJ">{{ __('Certificate') }}</a></p>
        <p style="margin-bottom:-0.3rem;">{{ __('Google') }}</p>
        <p style="margin-bottom:-0.3rem;">{{ __('Understand the importance of cybersecurity practices and their impact on organizations.') }}</p>
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:-1rem;margin-top:1.5rem;">{{ __('Master in PHP, SQL, POO, MVC, Laravel, Symfony, WordPress') }} | <a href="https://www.udemy.com/certificate/UC-45980f0e-28ad-4575-b419-3f93777d9fa8/">{{ __('Certificate') }}</a></p>
        <p style="margin-bottom:-0.3rem;">{{ __('Udemy') }}</p>
        <p style="margin-bottom:-0.3rem;">{{ __('Learn PHP from scratch, databases, SQL, MySQL, POO, MVC, Laravel 6, Symfony 6, WordPress') }}</p>
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:-1rem;margin-top:1.5rem;">{{ __('Master in JavaScript: Aprender JS, jQuery, Angular 8, NodeJS') }} | <a href="https://www.udemy.com/certificate/UC-5C1K9ARY/">{{ __('Certificate') }}</a></p>
        <p style="margin-bottom:-0.3rem;">{{ __('Udemy') }}</p>
        <p style="margin-bottom:-0.3rem;">{{ __('Learn how to program from scratch and web development with JavaScript, jQuery, JSON, TypeScript, Angular, Node, MEAN') }}</p>
    </div>
    <div style="height:2px;background-color:#5b7b30;margin-top:1rem;"></div>
    <div style="font-size:14px;margin-top:0.5rem;text-transform:uppercase;font-weight:bold;text-align:center;">
        {{ __('Projects') }}
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:0.3rem;">AlojaTuWeb | <a href="https://alojatuweb.com">https://alojatuweb.com</a></p>
        <p style="margin-bottom:-0.5rem;">{{ __('E-commerce development with Php7.4, BootstrapCSS(3.3), JQuery(1.12) and MySQL(5.6).') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Implementation of WHMCS for web hosting billing.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Registration and configuration of domains and dns in own vps with Ubuntu 20.04.') }}</p>
        <p style="color:#5b7b30;font-size:0.9rem;">{{ __('Currently, it is being built from scratch with Laravel 11.') }}</p>
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:0.3rem;">API Chiquitadas | <a href="https://chiquitadas.es">https://chiquitadas.es</a></p>
        <p style="margin-bottom:-0.5rem;">{{ __('Development of a REST API with Laravel 10(Php8.1), TailwindCSS(3.2), Livewire(2.12) and MySQL8.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('API documentation with OpenAPI 3.1.0 specification.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Domain and dns configuration on my own vps with Ubuntu 22.04.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Frontend design and layout.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Version management with git and git-flow in gitlab.') }}</p>
    </div>
    <div style="font-size:14px;">
        <p style="text-align:center;margin-bottom:0.3rem;margin-top:2rem;">Webmasterbrak | <a href="https://webmasterbrak.dev">https://webmasterbrak.dev</a></p>
        <p style="margin-bottom:-0.5rem;">{{ __('Personal portfolio developed with Laravel 10(Php8.2), TailwindCSS(3.4), Livewire(3.4) and MariaDB10.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Domain and dns configuration on my own vps with Ubuntu 22.04.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Frontend design and layout.') }}</p>
        <p style="margin-bottom:-0.5rem;">{{ __('Version management with git and git-flow in gitlab.') }}</p>
        <p style="color:#5b7b30;font-size:0.9rem;">{{ __('In this website you can also see the projects I am currently developing.') }}</p>
    </div>
    <div style="height:2px;background-color:#5b7b30;"></div>
    <div style="font-size:14px;font-weight:bold;margin-top:0.5rem;margin-bottom:0.5rem;text-transform:uppercase;text-align:center">
        {{ __('Digital soft skills') }}
    </div>
    <div style="font-size:14px;">
        <ul style="list-style-type:none;display:inline;text-align:center;">
            <li style="list-style-type:none;display:inline;">{{ __('Teamwork') }} |</li>
            <li style="list-style-type:none;display:inline;">{{ __('Self-learning capacity') }} |</li>
            <li style="list-style-type:none;display:inline;">{{ __('Troubleshooting') }} |</li>
            <li style="list-style-type:none;display:inline;">{{ __('Adaptability') }} |</li>
            <li style="list-style-type:none;display:inline;">{{ __('Critical thinking') }}</li>
        </ul>
    </div>
    <div style="font-size:14px;font-weight:bold;margin-top:0.5rem;margin-bottom:0.5rem;text-transform:uppercase;text-align:center">
        {{ __('Digital hard skills') }}
    </div>
    <div style="font-size:14px;">
        <ul style="list-style-type:none;display:inline;text-align:center;">
            <li style="list-style-type:none;display:inline;">PHP |</li>
            <li style="list-style-type:none;display:inline;">Laravel |</li>
            <li style="list-style-type:none;display:inline;">Symfony |</li>
            <li style="list-style-type:none;display:inline;">Slim |</li>
            <li style="list-style-type:none;display:inline;">HTML |</li>
            <li style="list-style-type:none;display:inline;">CSS |</li>
            <li style="list-style-type:none;display:inline;">BootstrapCSS |</li>
            <li style="list-style-type:none;display:inline;">TailwindCSS |</li>
            <li style="list-style-type:none;display:inline;">Git |</li>
            <li style="list-style-type:none;display:inline;">GitLab |</li>
            <li style="list-style-type:none;display:inline;">GitHub |</li>
            <li style="list-style-type:none;display:inline;">Bitbucket |</li>
            <li style="list-style-type:none;display:inline;">Linux</li>
            <li style="list-style-type:none;display:inline;">API REST |</li>
            <li style="list-style-type:none;display:inline;">Postman |</li>
            <li style="list-style-type:none;display:inline;">JavaScript |</li>
            <li style="list-style-type:none;display:inline;">AngulaJS |</li>
            <li style="list-style-type:none;display:inline;">Angular |</li>
            <li style="list-style-type:none;display:inline;">AlpineJS |</li>
            <li style="list-style-type:none;display:inline;">SQL |</li>
            <li style="list-style-type:none;display:inline;">MySQL |</li>
            <li style="list-style-type:none;display:inline;">MariaDb |</li>
            <li style="list-style-type:none;display:inline;">MongoDb</li>
            <li style="list-style-type:none;display:inline;">{{ __('Servers') }} |</li>
            <li style="list-style-type:none;display:inline;">{{ __('Domains') }} |</li>
            <li style="list-style-type:none;display:inline;">Plesk |</li>
            <li style="list-style-type:none;display:inline;">Cpanel |</li>
            <li style="list-style-type:none;display:inline;">DNS |</li>
            <li style="list-style-type:none;display:inline;">VPS |</li>
            <li style="list-style-type:none;display:inline;">Bash |</li>
            <li style="list-style-type:none;display:inline;">Apache |</li>
            <li style="list-style-type:none;display:inline;">Nginx |</li>
            <li style="list-style-type:none;display:inline;">Ssh |</li>
            <li style="list-style-type:none;display:inline;">Ftp |</li>
            <li style="list-style-type:none;display:inline;">Docker</li>
        </ul>
    </div>
</body>
</html>
